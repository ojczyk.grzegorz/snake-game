import turtle
import time
import random
#SET UP THE SCREEN
wndw = turtle.Screen()
wndw.title("Snake Game")
wndw.bgcolor("green")
wndw.setup(width=600, height=600)
wndw.tracer(0) #TURNS OFF THE SCREEN UPDATES

delay = 0.1
#SCORE
score = 0
high_score = 0

#SNAKE HEAD
head = turtle.Turtle()
head.speed(0)
head.shape("square")
head.color("black")
head.penup()
head.goto(0,0)
head.direction = "stop"

segments = [head]

#SNAKE FOOD
food = turtle.Turtle()
food.speed(0)
food.shape("circle")
food.color("red")
food.penup()
food.goto(0,100)
food .direction = "stop"

#PEN
pen = turtle.Turtle()
pen.speed(0)
pen.shape("square")
pen.color("white")
pen.penup()
pen.hideturtle()
pen.goto(0, 260)
def pen_write():
    global pen, score, high_score
    pen.clear()
    pen.write("Score: {} Hight Score: {}".format(score, high_score),\
                    align="center", font=("Courier", 24, "normal"))
pen_write()

#FUNCTIONS
def goUp():
    if head.direction != "down":
        head.direction = "up"
def goDown():
    if head.direction != "up":
        head.direction = "down"
def goRight():
    if head.direction != "left":
        head.direction = "right"
def goLeft():
    if head.direction != "right":
        head.direction = "left"
def move():
    if head.direction == "up":
        y = head.ycor()
        head.sety(y+20)
    elif head.direction == "down":
        y = head.ycor()
        head.sety(y - 20)
    elif head.direction == "right":
        x = head.xcor()
        head.setx(x + 20)
    elif head.direction == "left":
        x = head.xcor()
        head.setx(x - 20)
def gameEnd():
    time.sleep(1)
    global head, segments, score
    head.goto(0, 0)
    head.direction = "stop"
    for segment in segments[1:]:
        segment.goto(10000, 1000)
    segments = [head]
    score = 0
    pen_write()

wndw.listen()
wndw.onkeypress(goUp,"w")
wndw.onkeypress(goDown,"s")
wndw.onkeypress(goRight,"d")
wndw.onkeypress(goLeft,"a")
#MAIN GAME LOOP
while True:
    wndw.update()
    #CHECK FOR COLLISION WITH THE BORDER
    if head.xcor()>290 or head.xcor()<-290 or head.ycor()>290 or head.ycor()<-290:
        gameEnd()
    if head.distance(food) < 20:
        score += 1
        if score > high_score:
            high_score = score
        pen_write()
        #MOVE FOOD TO A RANDOM SPOT
        x = random.randint(-290,290)
        y = random.randint(-290,290)
        food.goto(x,y)
        #ADD A SEGMENT
        new_segment = turtle.Turtle()
        new_segment.speed(0)
        new_segment.shape("square")
        new_segment.color("grey")
        new_segment.penup()
        segments.append(new_segment)

    #MOVE THE END SEGMENTS FIRST IN REVERSE ORDER
    for idx in range(len(segments)-1,0,-1):
        x = segments[idx-1].xcor()
        y = segments[idx-1].ycor()
        segments[idx].goto(x,y)
    move()
    #CHECK FPR HEAD COLLISION WITH THE BODY SEGMENTS
    for segment in segments[1:]:
        if segment.distance(head) < 20:
            gameEnd()
    time.sleep(delay)
wndw.mainloop()